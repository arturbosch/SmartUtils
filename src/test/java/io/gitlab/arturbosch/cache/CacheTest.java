package io.gitlab.arturbosch.cache;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * @author Artur
 */
public class CacheTest {

	private static class TokenCache extends Cache<String, String> {
	}

	private Cache<String, String> cache;

	@Before
	public void setUp() {
		cache = new TokenCache();
		cache.getInternalCache().put("Hello", "user");
		cache.getInternalCache().put("World", "name");
	}

	@Test
	public void testReset() throws Exception {
		cache.reset();

		assertThat(cache.size(), is(0));
	}

	@Test
	public void testVerifyAndReturnUserId() throws Exception {
		assertThat(cache.getOrDefault("Hello"), is("user"));
		assertThat(cache.getOrDefault("World"), is("name"));
	}

	@Test
	public void testPutTokenWithUserId() throws Exception {
		cache.put("String", "artur");

		assertThat(cache.getInternalCache().containsKey("String"), is(true));
	}

	@Test(expected = NullPointerException.class)
	public void putNullTokenExpectException() {
		cache.put(null, "artur");
	}

	@Test(expected = NullPointerException.class)
	public void putNullUserIdExpectException() {
		cache.put("String", null);
	}

	@Test
	public void testHasToken() throws Exception {
		assertThat(cache.hasKey("World"), is(true));
	}

	@Test
	public void testHasValue() throws Exception {
		assertThat(cache.hasValue("user"), is(true));
		assertThat(cache.hasValue("blabla"), is(false));
	}
}
