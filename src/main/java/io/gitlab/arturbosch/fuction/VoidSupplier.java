package io.gitlab.arturbosch.fuction;

/**
 * Represents a supplier of no result. Given code block will just be invoked.
 *
 * @author artur
 */
@FunctionalInterface
public interface VoidSupplier {

	/**
	 * Invokes given code block.
	 */
	void invoke();
}
